require 'rails_helper'
require Rails.root.join('spec','shared_examples', 'error_structure')
require Rails.root.join('spec','shared_examples', 'success_structure')

RSpec.describe UsersController, type: :controller do

  describe "POST create" do

    context 'when some inputs are missing' do
      before do
        post :create, :params => { :user => { :name => "Any name"}}
      end
      it_behaves_like 'an error object'
    end

    context 'when all inputs are perfect' do
      before do
        post :create, :params => { :user => { :name => "Any name",
                                              :email => "user@example.com",
                                              :mobile => "01004100209",
                                              :password => "123456",
                                              :password_confirmation => "123456"
                                              }}
      end

      it_behaves_like 'a successful response'
    end
  end
end
