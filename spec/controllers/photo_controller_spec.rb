require 'rails_helper'

RSpec.describe PhotoController, type: :controller do

  describe "POST add_photo" do

    # login to http basic auth
    include AuthHelper
    before(:each) do
      http_login
    end

    context 'when some inputs are missing' do
      before do
        post :add_photo, :params => { :photo => { :path => "Any name"}}
      end
      it_behaves_like 'an error object'
    end

    context 'when all inputs are perfect' do
      before do
        post :add_photo, :params => { :photo => { :name => "Any name",
                                                  :path => "whatever/a/a.png"
                                                  }}
      end

      it_behaves_like 'a successful response'
    end
  end


end
