require 'rails_helper'
require Rails.root.join('spec','shared_examples', 'error_structure')
require Rails.root.join('spec','shared_examples', 'success_structure')

RSpec.describe AuthenticationController, type: :controller do

  describe "POST authenticate" do
    fixtures :users
    before do
      existing_user = users(:foo)
      post :authenticate, :params => {
        :email => existing_user['email'],
        :password => '123456'
      }
    end
    it_behaves_like 'a successful response'
  end

  describe "POST authenticate for non activated user" do
    fixtures :users
    before do
      unactivated = users(:not_activated)
      post :authenticate, :params => {
        :email => unactivated['email'],
        :password => '123456'
      }
    end
  end
end
