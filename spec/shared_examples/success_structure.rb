RSpec.shared_examples 'a successful response' do

  it "it has the correct success response structure" do
    parsed_response = JSON.parse(response.body)
    expect(parsed_response.keys).to contain_exactly('status','data')
    expect(parsed_response['status']).to eq("success")
  end
end
