RSpec.shared_examples 'an error object' do

  it "it has the correct error structure" do
    parsed_response = JSON.parse(response.body)
    expect(parsed_response.keys).to contain_exactly('status','error')
    expect(parsed_response['status']).to eq("error")
    expect(parsed_response['error'].keys).to contain_exactly('name','status','data')
    parsed_response['error']['data'].each do |single_error_body|
    	expect(single_error_body.keys).to contain_exactly('message','fieldName')
    end
  end
end
