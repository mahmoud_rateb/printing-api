# Preview all emails at http://localhost:3000/rails/mailers/call_me
class CallMePreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/call_me/call_me_to_admins
  def call_me_to_admins
  	user = User.first
    CallMeMailer.call_me_to_admins(user)
  end

end
