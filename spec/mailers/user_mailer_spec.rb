require "rails_helper"

RSpec.describe UserMailer, type: :mailer do
  describe "account_activation" do
    fixtures :users
    before do
      existing_user = users(:foo)
      existing_user.activation_token = User.new_token
    end
    let(:mail) { UserMailer.account_activation(users(:foo)) }


    it "renders the headers" do
      abort(mail.inspect)
      expect(mail.subject).to eq("Account activation")
      expect(mail.to).to eq(existing_user['email'])
      expect(mail.from).to eq(["noreply@printpicsnow.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

  describe "password_reset" do
    let(:mail) { UserMailer.password_reset }

    it "renders the headers" do
      expect(mail.subject).to eq("Password reset")
      expect(mail.to).to eq(["to@example.org"])
      expect(mail.from).to eq(["noreply@printpicsnow.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

end
