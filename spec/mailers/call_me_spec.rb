require "rails_helper"

RSpec.describe CallMeMailer, type: :mailer do
  
  describe "call_me_to_admins" do
    fixtures :users
    let(:mail) { CallMeMailer.call_me_to_admins(users(:foo)) }

    it "renders the headers" do
      expect(mail.subject).to eq("Request for call")
      expect(mail.to).to eq(["rateb@aucegypt.edu"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

end
