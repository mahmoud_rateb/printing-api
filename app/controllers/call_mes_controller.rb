class CallMesController < ApplicationController

  def create
    @current_user.callmes.create([:status => 1])
    CallMeMailer.call_me_to_admins(@current_user).deliver_now
    render_json_success(@current_user.callmes)
  end

  def call_me_params
    params.permit(:photos => [:name, :path])
  end
end
