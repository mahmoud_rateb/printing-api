class AccountActivationsController < ActionController::Base
  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(params[:id])
      user.activate
      @msg = "Account has been activated"
    else
      @msg = "This link is no longer valid"
    end
  end
end