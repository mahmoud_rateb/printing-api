class AuthenticationController < ApplicationController
  skip_before_action :authenticate_request, :only => [:authenticate]

  def authenticate
    command = AuthenticateUser.call(params[:email],params[:password])

    if command.success?
      token_body = { auth_token: command.result}
      render_json_success token_body
    else
      render json: {error: command.errors}, status: :unauthorized
    end
  end
end
