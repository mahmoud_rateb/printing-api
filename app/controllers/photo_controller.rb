class PhotoController < ApplicationController

  def add_photos
    photo_params["photos"].each do |uploaded_photo_path|
      @current_user.photos.create(uploaded_photo_path)
    end
    render_json_success(@current_user.photos)
  end

  def photo_params
    params.permit(:photos => [:name, :path])
  end
end
