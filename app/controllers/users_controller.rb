class UsersController < ApplicationController
  skip_before_action :authenticate_request, :only => [:create]
  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      render_json_success @user
    else
      render_json_validation_errors @user
    end
  end

  def show
  end

  def update
  end

  def delete
  end

  def user_params
    params.require(:user).permit(:name, :email,
                                 :password, :password_confirmation, :mobile)
  end
end
