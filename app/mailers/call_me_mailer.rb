class CallMeMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.call_me_mailer.call_me_to_admins.subject
  #
  def call_me_to_admins(user)
  	@user = user
    @greeting = "Hi"

    mail to: ["eslam.mahgoub@gmail.com","rateb@aucegypt.edu"], subject: "Request for call"
  end
end
