class AuthenticateUser
  prepend SimpleCommand

  def initialize(email,password)
    @email = email
    @password = password
  end

  def call
    JsonWebToken.encode(user_id: user.id) if user
  end

  private

  attr_accessor :email, :password

  def user
    user = User.find_by_email(email)
    if(user && !user.activated?)
      error_msg = "Account has not been activated. "
      error_msg += "Check your email for the activation link."
      errors.add :user_authentication, error_msg
    elsif user && user.authenticate(password)
      return user
    else
      errors.add :user_authentication, 'invalid credentials'
      nil
    end
  end
end
