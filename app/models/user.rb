class User < ApplicationRecord
  attr_accessor :activation_token
  before_save :downcase_email
  before_create :create_activation_digest

  # Validation
  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true,
    format: { with: VALID_EMAIL_REGEX },
    uniqueness: true
  VALID_PHONE_NUMBERS_REGEX = /\A01[0-2]{1}[0-9]{8}/
  validates :mobile, presence: true,
    format: { with: VALID_PHONE_NUMBERS_REGEX },
    uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: {minimum: 6 }, allow_nil: true

  has_secure_password

  has_many :photos
  has_many :callmes

  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
      BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def activate
    update_attribute(:activated, true)
    update_attribute(:activated_at, Time.zone.now)
  end

  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def authenticated?(activation_token)
    return false if activation_digest.nil?
    BCrypt::Password.new(activation_digest).is_password?(activation_token)
  end

  private

  def downcase_email
    self.email = email.downcase
  end

  def create_activation_digest
    self.activation_token = User.new_token
    self.activation_digest = User.digest(activation_token)
  end

end
