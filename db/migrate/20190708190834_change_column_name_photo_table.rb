class ChangeColumnNamePhotoTable < ActiveRecord::Migration[5.1]
  def change
  	rename_column :photos, :photo, :name
  end
end
