Rails.application.routes.draw do

  post 'add_photos', to: 'photo#add_photos'
  resources :users
  resources :call_mes
  post 'authenticate', to: 'authentication#authenticate'

  resources :account_activations, only: [:edit]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
