require 'test_helper'

class AuthenticationControllerTest < ActionDispatch::IntegrationTest
  test "Sending no credentials to authenticate" do
    post authenticate_path
    assert_response 401
  end

end
