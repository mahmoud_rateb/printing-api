require 'test_helper'

class UserTest < ActiveSupport::TestCase
	def setup
		@user = User.new(name: "Mahmoud Rateb", email:"rateb@hotmail.com", 
			mobile: "01004110219", password: "foobar", password_confirmation: "foobar") 
	end

	test "Should be valid" do
		assert @user.valid?
	end

	test "Name should be present" do
		@user.name = "    "
		assert_not @user.valid?
	end

	test "Email should be present" do 
		@user.email = "   "
		assert_not @user.valid?
	end

	test "Email should be of the proper email format" do
		invalid_addresses = %w[user@example,com user_at_foo.org user.name@example. foo@bar_baz.com foo@bar+baz.com]
		invalid_addresses.each do |invalid_address|
			@user.email = invalid_address
			assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
		end 
	end

	test "Name should not be too long" do
		@user.name = "a"*51
		assert_not @user.valid?
	end

	test "Mobile numbers should be of the correct format" do
		invalid_numbers = %w[ 0101 0225772800 19991 abc 0101431254]
		invalid_numbers.each do |invalid_number|
			@user.mobile = invalid_number
			assert_not @user.valid?, "#{invalid_number.inspect} is not a valid Egyptian mobile number"
		end
	end

	test "Mobile numbers should be unique" do
		duplicate_user = @user.dup
		@user.save
		assert_not duplicate_user.valid?
	end

	test "Email Addresses should be unique" do
		duplicate_user = @user.dup
		@user.save
		assert_not duplicate_user.valid?
	end

	test "Email Addresses should be unique. And case insensitive" do
		duplicate_user = @user.dup
		duplicate_user.email = @user.email.upcase
		@user.save
		assert_not duplicate_user.valid?
	end

	test "Email address should be saved in lowercase letters" do 
		mixed_case_email = "MAHMOUD@student.guc.edu.eg"
		@user.email = mixed_case_email
		@user.save
		assert_equal mixed_case_email.downcase, @user.reload.email
	end

	test "Password cannot be blank" do
		@user.password = @user.password_confirmation = " "*6
		assert_not @user.valid?
	end

	test "Password should have a minimum length" do
		@user.password = @user.password_confirmation = "a"*5
		assert_not @user.valid?
	end

end
